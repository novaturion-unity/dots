﻿using Unity.Entities;

namespace SolarDefender.Components
{
	[System.Serializable]
	public struct LinearSpeed : IComponentData
	{
		[UnityEngine.SerializeField] private float _defaultValue;
		public float value;

		public float DefaultValue { get => _defaultValue; }

		public LinearSpeed(float defaultValue)
		{
			_defaultValue = this.value = defaultValue;
		}

		public LinearSpeed(float defaultValue, float value)
		{
			_defaultValue = defaultValue;
			this.value = value;
		}
	}
}