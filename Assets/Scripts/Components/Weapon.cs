using SolarDefender.Interfaces;
using SolarDefender.Templates;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Extensions;
using Unity.Transforms;

namespace SolarDefender.Components
{
	public struct Weapon : IComponentData, IWeapon
	{
		public Translation shellSpawnPosition;

		[UnityEngine.Space]

		public Cooldown cooldown;

		public ShotForce shotForce;

		public Damage damage;

		public Weapon(SolarDefender.Templates.WeaponTemplate template)
		{
			shellSpawnPosition = template.shellSpawnPosition;

			cooldown = template.cooldown;
			shotForce = template.shotForce;
			damage = template.damage;
		}

		public Weapon(float3 shellSpawnPosition, float cooldown, float shotForce, float damage)
		{
			this.shellSpawnPosition.Value = shellSpawnPosition;

			this.cooldown = new Cooldown(cooldown);
			this.shotForce = new ShotForce(shotForce);
			this.damage = new Damage(damage);
		}

		public void Shoot(ref EntityManager entityManager, ref Entity shell)
		{
			Entity shellCopy = entityManager.Instantiate(shell);
			PhysicsVelocity velocity = entityManager.GetComponentData<PhysicsVelocity>(shellCopy);
			PhysicsMass mass = entityManager.GetComponentData<PhysicsMass>(shellCopy);

			entityManager.SetComponentData<Damage>(shellCopy, damage);
			ComponentExtensions.ApplyLinearImpulse(ref velocity, in mass, shotForce.value);
		}

		public void Shoot(ref EntityManager entityManager, ShellTemplate shellTemplate)
		{
			Entity shell = shellTemplate.GetEntity(ref entityManager);
			PhysicsVelocity velocity = entityManager.GetComponentData<PhysicsVelocity>(shell);
			PhysicsMass mass = entityManager.GetComponentData<PhysicsMass>(shell);

			entityManager.SetComponentData<Damage>(shell, damage);
			ComponentExtensions.ApplyLinearImpulse(ref velocity, in mass, shotForce.value);
		}
	}
}