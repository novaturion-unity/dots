﻿using Unity.Entities;

namespace SolarDefender.Components
{
	[System.Serializable]
	public struct Damage : IComponentData
	{
		[UnityEngine.SerializeField] private float _defaultValue;
		public float value;

		public float DefaultValue { get => _defaultValue; }

		public Damage(float defaultValue)
		{
			_defaultValue = this.value = defaultValue;
		}

		public Damage(float defaultValue, float value)
		{
			_defaultValue = defaultValue;
			this.value = value;
		}
	}
}