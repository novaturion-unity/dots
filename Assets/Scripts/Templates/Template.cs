﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Unity.Entities;

namespace SolarDefender.Templates
{
	public abstract class Template : UnityEngine.ScriptableObject
	{
		protected abstract void InitializeComponents();

		#region Internal

		protected virtual void OnEnable()
		{
			InitializeComponents();
		}

		protected virtual IEnumerable<FieldInfo> GetFields(params Type[] interfaces)
		{
			FieldInfo[] fields =
				this.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

			if (interfaces is null)
			{ return default; }

			if (interfaces.Length == 0)
			{
				UnityEngine.Debug.LogWarning("Empty interfaces list");
				return default;
			}

			return fields
				.Where(field => field.FieldType
					.GetInterfaces()
					.Intersect(interfaces)
					.Count() != 0
				);
		}

		#endregion

		#region SyncAPI

		public virtual ComponentType[] GetComponentsTypes()
		{
			return GetFields(typeof(IComponentData), typeof(ISharedComponentData))
				.Select(field => (ComponentType)field.FieldType)
				.ToArray();
		}

		public virtual IComponentData[] GetComponentsData()
		{
			return GetFields(typeof(IComponentData))
				.Select(field => (IComponentData)field.GetValue(this))
				.ToArray();
		}

		public virtual ISharedComponentData[] GetSharedComponentsData()
		{
			return GetFields(typeof(ISharedComponentData))
				.Select(field => (ISharedComponentData)field.GetValue(this))
				.ToArray();
		}

		public virtual Entity GetEntity(EntityManager entityManager)
		{
			ComponentType[] types = GetComponentsTypes();
			IComponentData[] componentsData = GetComponentsData();
			ISharedComponentData[] sharedComponentsData = GetSharedComponentsData();

			Entity entity = entityManager.CreateEntity(types);
			entityManager.SetName(entity, this.name);

			for (int i = 0; i < componentsData.Length; i++)
			{
				dynamic component = componentsData[i];
				entityManager.SetComponentData(entity, component);
			}

			for (int i = 0; i < sharedComponentsData.Length; i++)
			{
				dynamic component = sharedComponentsData[i];
				entityManager.SetSharedComponentData(entity, component);
			}

			return entity;
		}

		#endregion

		#region AsyncAPI

		public virtual async Task<ComponentType[]> GetComponentsTypesAsync()
		{
			return await Task.Run(() => GetComponentsTypes()).ConfigureAwait(false);
		}

		public virtual async Task<IComponentData[]> GetComponentsDataAsync()
		{
			return await Task.Run(() => GetComponentsData()).ConfigureAwait(false);
		}
		public virtual async Task<ISharedComponentData[]> GetSharedComponentsDataAsync()
		{
			return await Task.Run(() => GetSharedComponentsData()).ConfigureAwait(false);
		}

		#endregion
	}
}