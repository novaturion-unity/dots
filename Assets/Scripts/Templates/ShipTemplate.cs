﻿// using SolarDefender.Components;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Rendering;
using Unity.Transforms;

namespace SolarDefender.Templates
{
	[UnityEngine.CreateAssetMenu(fileName = "NewShip", menuName = "Solar Defender/Entities/Ship", order = 0)]
	public class ShipTemplate : Template
	{
		public float3 translationValue = default;
		public float3 rotationValue = default;
		public float scaleValue = default;

		protected LocalToWorld _localToWorld = default;
		protected Translation _translation = default;
		protected Rotation _rotation = default;
		protected Scale _scale = default;

		[UnityEngine.Space]

		public float massValue = default;
		public float gravityFactorValue = default;
		public float linearDampingValue = default;
		public float angularDampingValue = default;
		public float3 linearVelocityValue = default;
		public float3 angularVelocityValue = default;

		protected PhysicsMass _mass = default;
		protected PhysicsGravityFactor _gravityFactor = default;
		protected PhysicsDamping _damping = default;
		protected PhysicsVelocity _velocity = default;
		protected PhysicsCollider _collider = default;

		[UnityEngine.Space]

		public RenderMesh renderMesh = default;
		protected RenderBounds _renderBounds = default;

		// [UnityEngine.Space]

		// public VisualEffect movementVFX = default;

		// [UnityEngine.Space]

		// public Health health = default;

		// public Movement movement = default;

		// [UnityEngine.Space]

		// protected WeaponTemplate[] _weaponsValues = default;

		// protected Weapons _weapons = default;

		protected override void InitializeComponents()
		{
			InitializeTransform();
			InitializePhysics();
			InitializeRendering();
		}

		protected void InitializeTransform()
		{
			quaternion rotationEuler = quaternion.Euler(rotationValue);

			_translation.Value = translationValue;
			_rotation.Value = rotationEuler;
			_scale.Value = scaleValue;

			// _localToWorld.Value = math.mul(new float4x4(rotationEuler, translationValue), float4x4.Scale(scaleValue));
		}

		protected unsafe void InitializePhysics()
		{
			NativeArray<float3> colliderVertices = new NativeArray<float3>(renderMesh.mesh.vertices.Length, Allocator.Temp);
			NativeArray<int3> colliderTriangles = new NativeArray<int3>((int)math.floor(renderMesh.mesh.triangles.Length / 3), Allocator.Temp);

			for (int i = 0; i < renderMesh.mesh.vertices.Length; i++)
			{
				colliderVertices[i] = renderMesh.mesh.vertices[i];
			}

			for (int i = 0; i < renderMesh.mesh.triangles.Length; i += 3)
			{
				colliderTriangles[(int)math.floor(i / 3f)] = new int3(
					renderMesh.mesh.triangles[i],
					renderMesh.mesh.triangles[i + 1],
					renderMesh.mesh.triangles[i + 2]
				);
			}

			BlobAssetReference<Collider> colliderBlobRef =
				MeshCollider.Create(
					colliderVertices,
					colliderTriangles
				);

			Collider* colliderPtr = (Collider*)colliderBlobRef.GetUnsafePtr();
			_collider.Value = colliderBlobRef;

			_mass = PhysicsMass.CreateDynamic(
				colliderPtr->MassProperties,
				massValue
			);

			_gravityFactor.Value = gravityFactorValue;

			_damping.Linear = linearDampingValue;
			_damping.Angular = angularDampingValue;

			_velocity.Linear = linearVelocityValue;
			_velocity.Angular = math.mul(
				math.inverse(colliderPtr->MassProperties.MassDistribution.Transform.rot),
				angularVelocityValue
			);

			colliderVertices.Dispose();
			colliderTriangles.Dispose();
		}

		protected void InitializeRendering()
		{
			_renderBounds.Value = renderMesh.mesh.bounds.ToAABB();
		}
	}
}