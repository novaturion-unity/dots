﻿using Unity.Entities;

namespace SolarDefender.Interfaces
{
	public interface ITag : IComponentData { }
}