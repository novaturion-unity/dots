using SolarDefender.Templates;
using Unity.Entities;

namespace SolarDefender.Interfaces
{
	public interface IWeapon
	{
		void Shoot(ref EntityManager entityManager, ref Entity shell);
		void Shoot(ref EntityManager entityManager, ShellTemplate shellTemplate);
	}
}